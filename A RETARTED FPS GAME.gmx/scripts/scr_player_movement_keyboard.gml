if keyboard_check(ord("W")) 
|| keyboard_check(vk_up) 
    motion_add(face_dir, spd);

if keyboard_check(ord("A"))  
    motion_add(face_dir+90, spd);

if keyboard_check(ord("D")) 
    motion_add(face_dir-90, spd);

if keyboard_check(ord("S")) || keyboard_check(vk_down) 
    motion_add(face_dir+180, spd);
