///scr_player_movement_gamepad()
var axishor = gamepad_axis_value(global.gamepad_device, gp_axislh);
var axisver = -gamepad_axis_value(global.gamepad_device, gp_axislv);

if (abs(axisver)>0.5) {
    motion_add(face_dir, spd*axisver);
}

if (abs(axishor)>0.5) {
    motion_add(face_dir-90, spd*axishor);
}
