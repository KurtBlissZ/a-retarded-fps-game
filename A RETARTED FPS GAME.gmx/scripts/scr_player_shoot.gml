var c;
var x1 = x;
var y1 = y;
if sprite_index == spr_sword {
    //show_debug_message(choose("swush", "Swwwush", "SWing", 'swush swush swush'));
    var r = 10;
    var d = 20;
}else { 
    //show_debug_message(choose("Bang","BAANNNNGG","PEW PEW PEW","banggg"));
    var r = 200;
    var d = 5;
}
var x2 = lengthdir_x(4, face_dir);
var y2 = lengthdir_y(4, face_dir);
repeat(r) {
    x1 += x2;
    y1 += y2;
    if collision_line(x, y, x1, y1, obj_wall, 0, 0) break;
    c = collision_line(x, y, x1, y1, obj_enemy, 0, 0);
    if c {
        with c {
            hp -= d;
            show_debug_message(hp);
        }
        break;
    }
}
